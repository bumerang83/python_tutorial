#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_for_loop.htm

fruits = ['banana', 'apple',  'mango']
for index in range(len(fruits)):
   print 'Current fruit :', fruits[index]

print "Good bye!"
