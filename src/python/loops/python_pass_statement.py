#!/usr/bin/env python2
# https://www.tutorialspoint.com/python/python_pass_statement.htm

for letter in 'Python': 
   if letter == 'h':
      pass
      print 'This is pass block'
   print 'Current Letter :', letter

print "Good bye!"