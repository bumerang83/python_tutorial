#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_while_loop.htm

count = 0
while (count < 9):
   print 'The count is:', count
   count = count + 1

print "Good bye!"
