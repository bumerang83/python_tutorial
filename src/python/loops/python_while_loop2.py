#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_while_loop.htm

count = 0
while count < 5:
   print count, " is  less than 5"
   count = count + 1
else:
   print count, " is not less than 5"
