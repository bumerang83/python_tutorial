# https://www.tutorialspoint.com/python/python_loops.htm

# https://www.tutorialspoint.com/python/python_while_loop.htm
from python_while_loop0 import count

# https://www.tutorialspoint.com/python/python_while_loop.htm
#from python_while_loop1 import var # commented - preventing countless loop

# https://www.tutorialspoint.com/python/python_while_loop.htm
from python_while_loop2 import count

# https://www.tutorialspoint.com/python/python_for_loop.htm
from python_for_loop0 import letter

# https://www.tutorialspoint.com/python/python_for_loop.htm
from python_for_loop1 import fruits

# https://www.tutorialspoint.com/python/python_for_loop.htm
from python_for_loop2 import num

# https://www.tutorialspoint.com/python/python_nested_loops.htm
from python_nested_loop import i

# https://www.tutorialspoint.com/python/python_break_statement.htm
from python_break_statement import letter

# https://www.tutorialspoint.com/python/python_continue_statement.htm
from python_continue_statement import letter

# https://www.tutorialspoint.com/python/python_pass_statement.htm
from python_pass_statement import letter