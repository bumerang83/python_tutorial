#!/usr/bin/python
# https://www.tutorialspoint.com/python/number_random.htm

import random

# First random number
print "random() : ", random.random()

# Second random number
print "random() : ", random.random()
