#!/usr/bin/python
# https://www.tutorialspoint.com/python/number_radians.htm

import math
 
print "radians(3) : ",  math.radians(3)
print "radians(-3) : ",  math.radians(-3)
print "radians(0) : ",  math.radians(0)
print "radians(math.pi) : ",  math.radians(math.pi)
print "radians(math.pi/2) : ",  math.radians(math.pi/2)
print "radians(math.pi/4) : ",  math.radians(math.pi/4)

print "constatnt math.pi : ",  math.pi
print "constant math.e : ",  math.e
