#!/usr/bin/python
# https://www.tutorialspoint.com/python/number_uniform.htm

import random

print "Random Float uniform(5, 10) : ",  random.uniform(5, 10)

print "Random Float uniform(7, 14) : ",  random.uniform(7, 14)
