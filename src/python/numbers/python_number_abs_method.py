#!/usr/bin/python

# https://www.tutorialspoint.com/python/number_abs.htm

var = -45
#print "abs(-45) : ", abs(-45)
print "abs(-45) : ", abs(var)
var = 100.12
#print "abs(100.12) : ", abs(100.12)
print "abs(100.12) : ", abs(var)
var = 119L
#print "abs(119L) : ", abs(119L)
print "abs(119L) : ", abs(var)