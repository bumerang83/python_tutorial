#!/usr/bin/python

# https://www.tutorialspoint.com/python/python_gui_programming.htm

# https://stackoverflow.com/questions/26702119/installing-tkinter-on-ubuntu-14-04
# sudo apt-get install python-tk
# In python 2 the package name is 'Tkinter' not 'tkinter'
import Tkinter

#if __name__ == "__main__":
    
#try:
# for Python2
# from Tkinter import *   ## notice capitalized T in Tkinter 
#except ImportError:
# for Python3
#  from tkinter import *   ## notice lowercase 't' in tkinter here
#print "Hello World"
top = Tkinter.Tk()
# Code to add widgets will go here...
top.mainloop()