#!/usr/bin/python           # This is server.py file

import socket               # Import socket module

# https://www.tutorialspoint.com/python/python_networking.htm

if __name__ == "__main__":
    print "Hello World"

s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.

s.connect((host, port))
print s.recv(1024)
s.close                     # Close the socket when done