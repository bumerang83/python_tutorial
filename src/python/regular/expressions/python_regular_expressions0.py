#!/usr/bin/python

import re

# https://www.tutorialspoint.com/python/python_reg_expressions.htm

# if started as an application
if __name__ == "__main__":
#    print "Hello World"

    line = "Cats are smarter than dogs"

matchObj = re.match( r'(.*) are (.*?) .*', line, re.M|re.I)

if matchObj:
   print "matchObj.group() : ", matchObj.group()
   print "matchObj.group(1) : ", matchObj.group(1)
   print "matchObj.group(2) : ", matchObj.group(2)
   print "matchObj.group(0) : ", matchObj.group(0) # the output as the first one
   print "matchObj.groups() : ", matchObj.groups()
else:
   print "No match!!"