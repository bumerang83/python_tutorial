#!/usr/bin/python

import re

# https://www.tutorialspoint.com/python/python_reg_expressions.htm

# if started as an application
if __name__ == "__main__":
#    print "Hello World"

    line = "Cats are smarter than dogs";

matchObj = re.match( r'dogs', line, re.M|re.I)
if matchObj:
   print "match --> matchObj.group() : ", matchObj.group()
else:
   print "No match!!"

searchObj = re.search( r'dogs', line, re.M|re.I)
if searchObj:
   print "search --> searchObj.group() : ", searchObj.group()
else:
   print "Nothing found!!"