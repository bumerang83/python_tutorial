#!/usr/bin/python

import re

# https://www.tutorialspoint.com/python/python_reg_expressions.htm

# if started as an application
if __name__ == "__main__":
#    print "Hello World"

    line = "Cats are smarter than dogs"

searchObj = re.search( r'(.*) are (.*?) .*', line, re.M|re.I)

if searchObj:
   print "matchObj.group() : ", searchObj.group()
   print "matchObj.group(1) : ", searchObj.group(1)
   print "matchObj.group(2) : ", searchObj.group(2)
   print "matchObj.group(0) : ", searchObj.group(0) # the output as the first one
   print "matchObj.groups() : ", searchObj.groups()
else:
   print "No match!!"