# https://www.tutorialspoint.com/python/arithmetic_operators_example.htm
from python_arithmetic_operators_example import a

# https://www.tutorialspoint.com/python/comparison_operators_example.htm
from python_comparison_operators_example import a

# https://www.tutorialspoint.com/python/assignment_operators_example.htm
from python_assignment_operators_example import a

# https://www.tutorialspoint.com/python/bitwise_operators_example.htm
from python_bitwise_operators_example import a

# https://www.tutorialspoint.com/python/membership_operators_example.htm
from python_membership_operators_example import a

# https://www.tutorialspoint.com/python/identity_operators_example.htm
from python_identity_operators_example import a

# https://www.tutorialspoint.com/python/operators_precedence_example.htm
from python_operators_precedence_example import a