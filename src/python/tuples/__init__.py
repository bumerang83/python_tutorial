# https://www.tutorialspoint.com/python/python_tuples.htm

# https://www.tutorialspoint.com/python/python_tuples.htm
from python_tuples1 import tup1

# https://www.tutorialspoint.com/python/python_tuples.htm
from python_tuples2 import tup

# https://www.tutorialspoint.com/python/python_tuples.htm
from python_tuples3 import x

# https://www.tutorialspoint.com/python/tuple_cmp.htm
from python_tuple_cmp_method import tuple1

# https://www.tutorialspoint.com/python/tuple_len.htm
from python_tuple_len_method import tuple1

# https://www.tutorialspoint.com/python/tuple_max.htm
from python_tuple_max_method import tuple1

# https://www.tutorialspoint.com/python/tuple_min.htm
from python_tuple_min_method import tuple1

# https://www.tutorialspoint.com/python/tuple_tuple.htm
from python_tuple_tuple_method import aList