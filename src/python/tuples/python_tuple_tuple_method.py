#!/usr/bin/python
# https://www.tutorialspoint.com/python/tuple_tuple.htm

aList = (123, 'xyz', 'zara', 'abc');
bList = [123, 'xyz', 'zara', 'abc'];
aTuple = tuple(aList)
bTuple = tuple(bList)

print "Tuple 'a' elements : ", aTuple
print "Tuple 'b' elements : ", bTuple
