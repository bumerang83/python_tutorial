#!/usr/bin/python
# https://www.tutorialspoint.com/python/tuple_len.htm

tuple1, tuple2 = (123, 'xyz', 'zara'), (456, 'abc')

print "First tuple length : ", len(tuple1)
print "Second tuple length : ", len(tuple2)
