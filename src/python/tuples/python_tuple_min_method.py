#!/usr/bin/python
# https://www.tutorialspoint.com/python/tuple_min.htm

tuple1, tuple2, tuple3 = (123, 'xyz', 'zara', 'abc'), (456, 700, 200), ('xyz', 'zara', 'abc')

print "min value element : ", min(tuple1)
print "min value element : ", min(tuple2)
print "min value element : ", min(tuple3)  # prints 'abc'
