#!/usr/bin/python
# https://www.tutorialspoint.com/python/dictionary_clear.htm

dict = {'Name': 'Zara', 'Age': 7};

print "Start Len : %d" %  len(dict)
dict.clear()
print "End Len : %d" %  len(dict)
