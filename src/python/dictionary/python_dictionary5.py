#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_dictionary.htm

#dict = {['Name']: 'Zara', 'Age': 7}  # ['Name'] as dictionary key is not allowed!
dict = {('Name'): 'Zara', 'Age': 7}  # ('Name') is a tuple, it is possible to use tuples in dictionary as a key

print "dict['Name']: ", dict['Name']
