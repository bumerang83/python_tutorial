#!/usr/bin/python
# https://www.tutorialspoint.com/python/dictionary_setdefault.htm

dict = {'Name': 'Zara', 'Age': 7}

print "Value : %s" %  dict.setdefault('Age', None)
print "Value : %s" %  dict.setdefault('Sex', None) # added new key-value pair to the dictionary
#print "dict : %s:" % str(dict)
#print "Equivalent String : %s" % str (dict)
