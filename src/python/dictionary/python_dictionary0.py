#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_dictionary.htm

dict = {'Name': 'Zara', 'Age': 7, 'Class': 'First'}

print "dict['Name']: ", dict['Name']
print "dict['Age']: ", dict['Age']
