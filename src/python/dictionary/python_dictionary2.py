#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_dictionary.htm

dict = {'Name': 'Zara', 'Age': 7, 'Class': 'First'}

dict['Age'] = 8; # update existing entry
dict['School'] = "DPS School"; # Add new entry, it will be the first element of the dictionary


print "dict['Age']: ", dict['Age']
print "dict['School']: ", dict['School']
print "dict: ", dict  # prints all the dictionary in curly braces
