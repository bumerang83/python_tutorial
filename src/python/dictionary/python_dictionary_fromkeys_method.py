#!/usr/bin/python
# https://www.tutorialspoint.com/python/dictionary_fromkeys.htm

seq = ('name', 'age', 'sex')

dict = dict.fromkeys(seq)
print "New Dictionary : %s" %  str(dict)

dict = dict.fromkeys(seq, 10)  # one value to all keys
print "New Dictionary : %s" %  str(dict)
