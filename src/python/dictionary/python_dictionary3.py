#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_dictionary.htm

var ='';
dict = {'Name': 'Zara', 'Age': 7, 'Class': 'First'}

print "Printing all the dictionary 'dict' first: ", dict

del dict['Name']; # remove entry with key 'Name'

print "Printing the dictionary 'dict' after removing ['Name']: ", dict

dict.clear();     # remove all entries in dict

print "Printing the dictionary 'dict' after clearing 'clear()' it: ", dict

del dict ;        # delete entire dictionary

print "Printing the dictionary 'dict' after deleting 'del' it: ", dict

#print "dict['Age']: ", dict['Age']
#print "dict['School']: ", dict['School']
