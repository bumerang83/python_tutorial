#!/usr/bin/python
# https://www.tutorialspoint.com/python/dictionary_has_key.htm

dict = {'Name': 'Zara', 'Age': 7}

print "Value : %s" %  dict.has_key('Age')
print "Value : %s" %  dict.has_key('Sex')
