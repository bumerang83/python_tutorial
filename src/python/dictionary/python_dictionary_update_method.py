#!/usr/bin/python
# https://www.tutorialspoint.com/python/dictionary_update.htm

dict = {'Name': 'Zara', 'Age': 7}
dict2 = {'Sex': 'female'}
dict2 = {'Sex': 'female',  'Age': 7} # the same result if key-pair already exist in dictionary, no dublicates!

dict.update(dict2)
print "Updated Value : %s" %  dict
