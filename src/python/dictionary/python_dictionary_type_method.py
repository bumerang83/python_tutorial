#!/usr/bin/python
# https://www.tutorialspoint.com/python/dictionary_str.htm

dict = {'Name': 'Zara', 'Age': 7};
print "Variable Type : %s" %  type (dict)  # types '<type 'dict'>'

#del dict
#print "Variable dict : %s" %  dict  # after deleting prints the same '<type 'dict'>'
