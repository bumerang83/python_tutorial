#!/usr/bin/python
# https://www.tutorialspoint.com/python/dictionary_str.htm

dict = {'Name': 'Zara', 'Age': 7};
print "Equivalent String : %s" % str (dict)
print "Prints pure dictionary 'dict' : %s" % dict
