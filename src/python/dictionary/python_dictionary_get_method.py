#!/usr/bin/python
# https://www.tutorialspoint.com/python/dictionary_get.htm


dict = {'Name': 'Zabra', 'Age': 7}

print "Value : %s" %  dict.get('Age')
print "Value : %s" %  dict.get('Education', "Never")
