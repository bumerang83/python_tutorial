#!/usr/bin/env python2
#encoding: UTF-8

# https://www.tutorialspoint.com/python/python_database_access.htm

# 22/11/2017, tried from terminal: $ sudo apt-get install python-pip python-dev libmysqlclient-devsudo 
# 22/11/2017, tried from terminal: $ sudo pip install MySQL-python

# also tried found solution from
# https://stackoverflow.com/questions/42221291/can-not-import-mysqldb-on-python-although-it-is-installed

# got the answer: Requirement already satisfied: MySQL-python in /usr/local/lib/python2.7/dist-packages
# at the end changed Project->Properties->Python:Python Platform from Jython 2.7.0 to Python 2.7.12 and everything works!
import MySQLdb

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

if __name__ == "__main__":
    print "Hello Python Mysql Database Access 0!"
    
# Open database connection
#db = MySQLdb.connect("localhost","testuser","test123","TESTDB" )
# USE 127.0.0.1 INSTEAD OF 'localhost'!
# https://stackoverflow.com/questions/18150858/operationalerror-2002-cant-connect-to-local-mysql-server-through-socket-v
db = MySQLdb.connect("127.0.0.1","root","","TESTDB" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

# Drop table if it already exist using execute() method.
cursor.execute("DROP TABLE IF EXISTS EMPLOYEE")

# Create table as per requirement
sql = """CREATE TABLE EMPLOYEE (
         FIRST_NAME  CHAR(20) NOT NULL,
         LAST_NAME  CHAR(20),
         AGE INT,  
         SEX CHAR(1),
         INCOME FLOAT )"""

cursor.execute(sql)

# disconnect from server
db.close()