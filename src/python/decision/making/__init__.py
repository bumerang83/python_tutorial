# https://www.tutorialspoint.com/python/python_decision_making.htm
from python_decision_making import var

# https://www.tutorialspoint.com/python/python_if_statement.htm
from python_if_statement import var1

# https://www.tutorialspoint.com/python/python_if_else.htm
from python_if_else0 import var1

# https://www.tutorialspoint.com/python/python_if_else.htm
from python_if_else1 import var

# https://www.tutorialspoint.com/python/nested_if_statements_in_python.htm
from python_nested_if_statements_in_python import var