#!/usr/bin/python

import smtplib
import base64

# https://www.tutorialspoint.com/python/python_sending_email.htm

if __name__ == "__main__":
    print "Hello World"
       
filename = "/tmp/test.txt"
#filename = "test.txt"

# Read a file and encode it into base64 format
#fo = open(filename, "rb")
fo = open(filename, "wb+")
filecontent = fo.read()
encodedcontent = base64.b64encode(filecontent)  # base64
    
#sender = 'from@fromdomain.com'
sender = 'andrei.lunin.0@gmail.com'
#receivers = ['to@todomain.com']
receivers = ['andrei.lunin.0@gmail.com']

marker = "AUNIQUEMARKER"

body ="""
This is a test email to send an attachement.
"""
# Define the main headers.
part1 = """From: From Person <me@fromdomain.net>
To: To Person <amrood.admin@gmail.com>
Subject: Sending Attachement
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary=%s
--%s
""" % (marker, marker)

# Define the message action
part2 = """Content-Type: text/plain
Content-Transfer-Encoding:8bit

%s
--%s
""" % (body,marker)

# Define the attachment section
part3 = """Content-Type: multipart/mixed; name=\"%s\"
Content-Transfer-Encoding:base64
Content-Disposition: attachment; filename=%s

%s
--%s--
""" %(filename, filename, encodedcontent, marker)
message = part1 + part2 + part3

try:
    # https://stackoverflow.com/questions/10147455/how-to-send-an-email-with-gmail-as-provider-using-python
#   smtpObj = smtplib.SMTP('localhost')
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login('andrei.lunin.0@gmail.com', '3830206Izplaneta350')
   smtpObj.sendmail(sender, receivers, message)         
   print "Successfully sent email"
except smtplib.SMTPException: # https://stackoverflow.com/questions/13115724/new-to-python-gmail-smtp-error
   print "Error: unable to send email"
