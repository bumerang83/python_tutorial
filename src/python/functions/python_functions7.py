#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_functions.htm

# Function definition is here
sum = lambda arg1, arg2: arg1 + arg2;
multy = lambda arg1, arg2: arg1 * arg2; # added by AL

# Now you can call sum as a function
print "Value of total : ", sum( 10, 20 )
print "Value of total : ", sum( 20, 20 )

print "Value of multy : ", multy( 20, 20 ) # added by AL
print "Value of multy : ", multy( 20, 40 ) # added by AL
