# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions0 import printme

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions1 import changeme

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions2 import changeme

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions3 import printme

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions4 import printinfo

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions5 import printinfo

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions6 import printinfo

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions7 import sum

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions8 import sum

# https://www.tutorialspoint.com/python/python_functions.htm
from python_functions9 import sum