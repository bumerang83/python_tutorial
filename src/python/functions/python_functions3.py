#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_functions.htm

# Function definition is here
def printme( str ):
   "This prints a passed string into this function"
   print str
   return;

# Now you can call printme function
printme( str = "My string")
