#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_functions.htm

# Function definition is here
def printinfo( name, age ):
   "This prints a passed info into this function"
   print "Name: ", name
   print "Age ", age
   return;

# Now you can call printinfo function
printinfo( age=50, name="miki" )
printinfo( age="miki", name=50 ) # the result is vice versa, but no error
