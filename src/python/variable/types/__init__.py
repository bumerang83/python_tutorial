# https://www.tutorialspoint.com/python/python_variable_types.htm
from python_variable_types0 import counter
from python_variable_types1 import str
from python_variable_types2 import tinytuple
from python_variable_types3  import tinydict