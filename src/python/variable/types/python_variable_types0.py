#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_variable_types.htm
counter = 100          # An integer assignment
miles   = 1000.0       # A floating point
name    = "John"       # A string

print counter
print miles
print name
