#!/usr/bin/python
# https://www.tutorialspoint.com/python/time_gmtime.htm

import time

t = time.gmtime()
print "time.gmtime() : %s" % str(t)
t = time.gmtime(10)
print "time.gmtime(10) : %s" % str(t)  # prints time tuple of the 1970, 1 january 00:00:00 + 10 seconds
