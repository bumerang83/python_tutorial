# https://www.tutorialspoint.com/python/python_date_time.htm
from python_date_time0 import ticks

# https://www.tutorialspoint.com/python/python_date_time.htm
from python_date_time1 import localtime

# https://www.tutorialspoint.com/python/python_date_time.htm
from python_date_time2 import localtime

# https://www.tutorialspoint.com/python/python_date_time.htm
from python_date_time3 import cal

# https://www.tutorialspoint.com/python/time_altzone.htm
from python_time_altzone_method import var

# https://www.tutorialspoint.com/python/time_asctime.htm
from python_time_asctime_method import t

# https://www.tutorialspoint.com/python/time_clock.htm
from python_time_clock_method import t0

# https://www.tutorialspoint.com/python/time_ctime.htm
from python_time_ctime_method import var

# https://www.tutorialspoint.com/python/time_gmtime.htm
from python_time_gmtime_method import t

# https://www.tutorialspoint.com/python/time_localtime.htm
from python_time_localtime_method import t

# https://www.tutorialspoint.com/python/time_mktime.htm
from python_time_mktime_method import t

# https://www.tutorialspoint.com/python/time_sleep.htm
from python_time_sleep_method import t

# https://www.tutorialspoint.com/python/time_strftime.htm
from python_time_strftime_method import t

# https://www.tutorialspoint.com/python/time_strptime.htm
from python_time_strptime_method import struct_time

# https://www.tutorialspoint.com/python/time_time.htm
from python_time_time_method import var

# https://www.tutorialspoint.com/python/python_date_time.htm
from python_date_calendar_module import var