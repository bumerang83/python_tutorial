#!/usr/bin/python
# https://www.tutorialspoint.com/python/time_asctime.htm

import time

t = time.localtime()
print "time.asctime(t): %s " % time.asctime(t)
