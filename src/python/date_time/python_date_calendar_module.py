#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_date_time.htm

import calendar;  # This is required to include 'calendar' module.
import time;  # This is required to include 'time' module.

# ticks = time.time()
# calendar(year, w=2, l=1, c=6, m=3)
#print "calendar.calendar(2016,w=2,l=1,c=6):", calendar.calendar(2016,w=2,l=1,c=6) # works without indent
#print "calendar.calendar(2016,w=2,l=1,c=6):", calendar.calendar(2016, w=2, l=1, c=6, m=3) # works without indent

var=1
#1
print(calendar.calendar(2017)) # works without indent

#2
print"First weekday: ", calendar.firstweekday( )

#3
print "Is leapyear:", calendar.isleap(2017)

#4
print "Number of leapdays in interval of 2001 and 2017", calendar.leapdays(2001, 2017)

#5
print "calendar.month(2017, 1, w=2, l=1)", calendar.month(2017,1,w=2,l=1)

#6
print "calendar.monthcalendar(2017,2)", calendar.monthcalendar(2017,2)

#7
print "calendar.monthrange(2017,2)", calendar.monthrange(2017,2)

#8
print "calendar.prcal(2017,w=2,l=1,c=6)", calendar.prcal(2017,w=2,l=1,c=6)

#9
print "calendar.prmonth(2017,month,w=2,l=1)", calendar.prmonth(2017,3,w=2,l=1)

#10
print "calendar.setfirstweekday(1)", calendar.setfirstweekday(0)

#11
print "calendar.timegm(tupletime)", calendar.timegm(time.localtime(time.time())) # time.localtime(time.time()) - gets time tuple

#12
print "calendar.weekday(year,month,day)", calendar.weekday(2017,9,20)
