#!/usr/bin/python
# https://www.tutorialspoint.com/python/time_sleep.htm

import time

t = 1
print "Start : %s" % time.ctime()
time.sleep( 5 )
print "End : %s" % time.ctime()
