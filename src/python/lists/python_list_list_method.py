#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_list.htm

aTuple = (123, 'xyz', 'zara', 'abc');
aList = list(aTuple)

print "List elements : ", aList
