#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_pop.htm

aList = [123, 'xyz', 'zara', 'abc'];

print "A List : ", aList.pop()
print "B List : ", aList.pop(2)

# below added by AL, 25/08/2017
aList = [123, 'xyz', 'zara', 'abc'];

print "A List : ", aList.pop()
print "B List : ", aList.pop(1)
