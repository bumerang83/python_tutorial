#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_append.htm

aList = [123, 'xyz', 'zara', 'abc'];
print "Defined List : ", aList
aList.append( 2009 );
print "Updated List : ", aList
