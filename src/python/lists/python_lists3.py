#!/usr/bin/python

# https://www.tutorialspoint.com/python/python_lists.htm

list1 = ['physics', 'chemistry', 1997, 2000];

print list1
del list1[2];
print "After deleting value at index 2 : "
print list1
