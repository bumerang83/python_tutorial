#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_reverse.htm

aList = [123, 'xyz', 'zara', 'abc', 'xyz'];

print "The Defined List : ", aList
aList.reverse();
print "The Reversed List : ", aList
