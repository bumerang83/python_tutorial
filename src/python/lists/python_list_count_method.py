#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_count.htm

aList = [123, 'xyz', 'zara', 'abc', 123];

print "Count for 123 : ", aList.count(123)
print "Count for zara : ", aList.count('zara')
