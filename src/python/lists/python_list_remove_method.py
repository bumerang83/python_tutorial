#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_remove.htm

aList = [123, 'xyz', 'zara', 'abc', 'xyz'];

aList.remove('xyz');  # the first occurrence of 'xyz' from the left will be removed
print "List : ", aList
aList.remove('abc');
print "List : ", aList
