#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_insert.htm

aList = [123, 'xyz', 'zara', 'abc']

print "Defined List : ", aList

aList.insert( 3, 2009)

print "Final List : ", aList
