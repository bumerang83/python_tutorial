#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_min.htm

list1, list2 = [123, 'xyz', 'zara', 'abc', 1], [456, 700, 200]

print "min value element : ", min(list1)
print "min value element : ", min(list2)
