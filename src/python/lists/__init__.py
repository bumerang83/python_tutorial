# https://www.tutorialspoint.com/python/python_lists.htm
from python_lists1 import list1

# https://www.tutorialspoint.com/python/python_lists.htm
from python_lists2 import list

# https://www.tutorialspoint.com/python/python_lists.htm
from python_lists3 import list1

# https://www.tutorialspoint.com/python/list_cmp.htm
from python_list_cmp_method import list1

# https://www.tutorialspoint.com/python/list_len.htm
from python_list_len_method import list1

# https://www.tutorialspoint.com/python/list_max.htm
from python_list_max_method import list1

# https://www.tutorialspoint.com/python/list_min.htm
from python_list_min_method import list1

# https://www.tutorialspoint.com/python/list_list.htm
from python_list_list_method import aTuple

# https://www.tutorialspoint.com/python/list_append.htm
from python_list_append_method import aList

# https://www.tutorialspoint.com/python/list_count.htm
from python_list_count_method import aList

# https://www.tutorialspoint.com/python/list_extend.htm
from python_list_extend_method import aList

# https://www.tutorialspoint.com/python/list_index.htm
from python_list_index_method import aList

# https://www.tutorialspoint.com/python/list_insert.htm
from python_list_insert_method import aList

# https://www.tutorialspoint.com/python/list_pop.htm
from python_list_pop_method import aList

# https://www.tutorialspoint.com/python/list_remove.htm
from python_list_remove_method import aList

# https://www.tutorialspoint.com/python/list_reverse.htm
from python_list_reverse_method import aList

# https://www.tutorialspoint.com/python/list_sort.htm
from python_list_sort_method import aList