#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_max.htm

list1, list2 = [123, 'xyz', 'zara', 'abc'], [456, 700, 200]

print "Max value element : ", max(list1)
print "Max value element : ", max(list2)
