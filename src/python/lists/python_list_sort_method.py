#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_sort.htm

aList = [123, 'xyz', 'zara', 'abc', 'xyz'];

print "The Defined List : ", aList
aList.sort();
print "The Sorted List : ", aList
