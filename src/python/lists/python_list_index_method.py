#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_index.htm

aList = [123, 'xyz', 'zara', 'abc'];

print "Index for xyz : ", aList.index( 'xyz' ) 
print "Index for zara : ", aList.index( 'zara' )
# print "Index for exception : ", aList.index( 'exception' )  # throwing an exception
