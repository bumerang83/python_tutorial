#!/usr/bin/python

# https://www.tutorialspoint.com/python/list_extend.htm

aList = [123, 'xyz', 'zara', 'abc', 123];
print "Defined List : ", aList 
bList = [2009, 'manni'];
aList.extend(bList)

print "Extended List : ", aList 
