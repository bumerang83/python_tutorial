#!/usr/bin/python

# https://www.tutorialspoint.com/python/string_max.htm

str = "this is really a string example....wow!!!";
print "Max character: " + max(str)

str = "this is a string example....wow!!!";
print "Max character: " + max(str)


str = "this is a string example....zoz!!!";  # 'z' will be the answer
print "Max character: " + max(str)
