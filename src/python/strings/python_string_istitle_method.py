#!/usr/bin/python

# https://www.tutorialspoint.com/python/string_istitle.htm

str = "This Is String Example...Wow!!!";
print str.istitle()

str = "This is string example....wow!!!";
print str.istitle()

str = "12";  # false if only a number/numbers
print str.istitle()
