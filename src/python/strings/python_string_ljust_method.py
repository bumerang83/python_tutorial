#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_ljust.htm

#!/usr/bin/python


str = "this is string example....wow!!!";

print str.ljust(50, '0')

str = "this is string example....wow!!!";

print str.ljust(100)  # padding with spaces, default padding character

str = "this is string example....wow!!!";

print str.ljust(20, '0')  # printing the string, 'len' is less than the original string length
