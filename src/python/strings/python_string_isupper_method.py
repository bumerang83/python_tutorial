#!/usr/bin/python

# https://www.tutorialspoint.com/python/string_isupper.htm

str = "THIS IS STRING EXAMPLE....WOW!!!"; 
print str.isupper()

str = "THIS is string example....wow!!!";
print str.isupper()