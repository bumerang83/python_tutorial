#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_isspace.htm

str = "       ";  # only spaces
print str.isspace()

str = "This is string example....wow!!!";  # alphabet characters and spaces
print str.isspace()

str = "	";  # only one 'tab' character
print str.isspace()


