#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_isalnum.htm

str = "this2009";  # No space in this string
print str.isalnum()

# str = "this is string example....wow!!!";
str = "thjgj hgdj";
print str.isalnum()
