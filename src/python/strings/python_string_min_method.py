#!/usr/bin/python

# https://www.tutorialspoint.com/python/string_min.htm

str = "this-is-real-string-example....wow!!!";
print "Min character: " + min(str)

str = "this-is-a-string-example....wow";  # '-' will be the answer
print "Min character: " + min(str)

str = "thisisastringexamplewow";  # 'a' will be the answer
print "Min character: " + min(str)
