#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_isdigit.htm

str = "123456";  # Only digit in this string
print str.isdigit()

str = "this is string example....wow!!!";
print str.isdigit()
