#!/usr/bin/python

# https://www.tutorialspoint.com/python/string_len.htm


str = "this is string example....wow!!!";

print "Length of the string: ", len(str)

str = "";

print "Length of the string: ", len(str)  # the result will be '0'
