# https://www.tutorialspoint.com/python/python_strings.htm

# https://www.tutorialspoint.com/python/python_strings.htm
from python_strings0 import var1

# https://www.tutorialspoint.com/python/python_strings.htm
from python_strings1 import var1

# https://www.tutorialspoint.com/python/python_strings.htm
from python_strings2 import var1

# https://www.tutorialspoint.com/python/python_strings.htm
from python_strings3 import var1

# https://www.tutorialspoint.com/python/python_strings.htm
from python_strings4 import var1

# https://www.tutorialspoint.com/python/python_strings.htm
from python_strings5 import var1

# https://www.tutorialspoint.com/python/string_capitalize.htm
from python_string_capitalize_method import str

# https://www.tutorialspoint.com/python/string_center.htm
from python_string_center_method import str

# https://www.tutorialspoint.com/python/string_count.htm
from python_string_count_method import str

# https://www.tutorialspoint.com/python/string_decode.htm
from python_string_decode_method import Str

# https://www.tutorialspoint.com/python/string_encode.htm
from python_string_encode_method import str

# https://www.tutorialspoint.com/python/string_endswith.htm
from python_string_endswith_method import str

# https://www.tutorialspoint.com/python/string_expandtabs.htm
from python_string_expandtabs_method import str

# https://www.tutorialspoint.com/python/string_find.htm
from python_string_find_method import str1

# https://www.tutorialspoint.com/python/string_index.htm
from python_string_index_method import str1

# https://www.tutorialspoint.com/python/string_isalnum.htm
from python_string_isalnum_method import str

# https://www.tutorialspoint.com/python/string_isalpha.htm
from python_string_isalpha_method import str

# https://www.tutorialspoint.com/python/string_isdigit.htm
from python_string_isdigit_method import str

# https://www.tutorialspoint.com/python/string_islower.htm
from python_string_islower_method import str

# https://www.tutorialspoint.com/python/string_isnumeric.htm
from python_string_isnumeric_method import str

# https://www.tutorialspoint.com/python/string_isspace.htm
from python_string_isspace_method import str

# https://www.tutorialspoint.com/python/string_istitle.htm
from python_string_istitle_method import str

# https://www.tutorialspoint.com/python/string_isupper.htm
from python_string_isupper_method import str

# https://www.tutorialspoint.com/python/string_join.htm
from python_string_join_method import s

# https://www.tutorialspoint.com/python/string_len.htm
from python_string_len_method import str

# https://www.tutorialspoint.com/python/string_ljust.htm
from python_string_ljust_method import str

# https://www.tutorialspoint.com/python/string_lower.htm
from python_string_lower_method import str

# https://www.tutorialspoint.com/python/string_lstrip.htm
from python_string_lstrip_method import str

# https://www.tutorialspoint.com/python/string_maketrans.htm
from python_string_maketrans_method import str

# https://www.tutorialspoint.com/python/string_max.htm
from python_string_max_method import str

# https://www.tutorialspoint.com/python/string_min.htm
from python_string_min_method import str

# https://www.tutorialspoint.com/python/string_replace.htm
from python_string_replace_method import str

# https://www.tutorialspoint.com/python/string_rfind.htm
from python_string_rfind_method import str1

# https://www.tutorialspoint.com/python/string_rindex.htm
from python_string_rindex_method import str1

# https://www.tutorialspoint.com/python/string_rjust.htm
from python_string_rjust_method import str

# https://www.tutorialspoint.com/python/string_rstrip.htm
from python_string_rstrip_method import str

# https://www.tutorialspoint.com/python/string_split.htm
from python_string_split_method import str

# https://www.tutorialspoint.com/python/string_splitlines.htm
from python_string_splitlines_method import str

# https://www.tutorialspoint.com/python/string_startswith.htm
from python_string_startswith_method import str

# https://www.tutorialspoint.com/python/string_strip.htm
from python_string_strip_method import str

# https://www.tutorialspoint.com/python/string_swapcase.htm
from python_string_swapcase_method import str

# https://www.tutorialspoint.com/python/string_title.htm
from python_string_title_method import str

# https://www.tutorialspoint.com/python/string_translate.htm
from python_string_translate_method import intab

# https://www.tutorialspoint.com/python/string_upper.htm
from python_string_upper_method import str

# https://www.tutorialspoint.com/python/string_zfill.htm
from python_string_zfill_method import str

# https://www.tutorialspoint.com/python/string_isdecimal.htm
from python_string_isdecimal_method import str