#!/usr/bin/python

# https://www.tutorialspoint.com/python/string_isdecimal.htm

str = u"this2009";  
print str.isdecimal();

str = u"23443434";
print str.isdecimal();