#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_lower.htm


str = "THIS IS STRING EXAMPLE....WOW!!!";

print str.lower()

str = "this";

print str.lower()  # result is the same, 'this'

str = "5";

print str.lower()  # result is the same, '5'


