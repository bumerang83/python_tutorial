#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_islower.htm

str = "THIS is string example....wow!!!"; 
print str.islower()

str = "this is string example....wow!!!";
print str.islower()

str = "";  # the result of 'islower()' is false if an empty string
print str.islower()


