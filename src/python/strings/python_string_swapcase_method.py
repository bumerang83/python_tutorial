#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_swapcase.htm

str = "this is string example....wow!!!";
print str.swapcase()

str = "THIS IS STRING EXAMPLE....WOW!!!";
print str.swapcase()
