#!/usr/bin/python

# https://www.tutorialspoint.com/python/string_maketrans.htm

from string import maketrans   # Required to call maketrans function.

intab = "aeiou"
outtab = "12345"
trantab = maketrans(intab, outtab)

str = "this is string example....wow!!!"
print str.translate(trantab)
