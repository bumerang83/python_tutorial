#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_decode.htm


Str = "this is string example....wow!!!";
Str = Str.encode('base64','strict');

print "Encoded String: " + Str
print "Decoded String: " + Str.decode('base64','strict')
