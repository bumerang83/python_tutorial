#!/usr/bin/python
# https://www.tutorialspoint.com/python/string_isnumeric.htm

str = u"this2009";  
print str.isnumeric()

str = u"23443434";
print str.isnumeric()
