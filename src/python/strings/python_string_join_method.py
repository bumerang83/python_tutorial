#!/usr/bin/python

# https://www.tutorialspoint.com/python/string_join.htm

s = "-";
seq = ("a", "b", "c"); # This is sequence of strings.
print s.join( seq )

s = "+";
seq = ("a", "b", "c"); # This is sequence of strings.
print s.join( seq )

s = " JOIN ";
seq = ("a", "b", "c"); # This is sequence of strings.
print s.join( seq )
