#!/usr/bin/python

# https://www.tutorialspoint.com/python/file_fileno.htm

# Open a file
fo = open("foo.txt", "wb")
print "Name of the file: ", fo.name

fid = fo.fileno()
#print "File Descriptor: %d" % fid
print "File Descriptor: ", fid

# Close opend file
fo.close()