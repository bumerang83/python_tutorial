#!/usr/bin/python

# https://www.tutorialspoint.com/python/os_chflags.htm

import os

#path = "/tmp/foo.txt"
path = ".\\tmp\\foo.txt"
# Set a flag so that file may not be renamed or deleted.
flags = os.SF_NOUNLINK # ERROR: chflags() is only available on systems that that support this particular system call, which does include most BSDs but does not include Windows
retval = os.chflags( path, flags) # https://stackoverflow.com/questions/30974283/in-python-2-7-not-able-to-change-flag-using-chflags-for-file-as-it-should-no
print "Return Value: %s" % retval