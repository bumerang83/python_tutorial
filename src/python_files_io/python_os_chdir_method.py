#!/usr/bin/python

# https://www.tutorialspoint.com/python/os_chdir.htm

import os

#path = "/usr/tmp"
path = "tmp"

# Check current working directory.
retval = os.getcwd()
print "Current working directory %s" % retval

# Now change the directory
#os.chdir( path )
os.chdir( retval + '\\' + path )  # at first created directory 'tmp' in the parent directory

# Check current working directory.
retval = os.getcwd()

print "Directory changed successfully %s" % retval