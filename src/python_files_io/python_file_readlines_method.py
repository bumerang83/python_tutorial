#!/usr/bin/python

# https://www.tutorialspoint.com/python/file_readlines.htm

# Open a file
#fo = open("foo.txt", "rw+") # ValueError: mode string must begin with one of 'r', 'w', 'a' or 'U', not 'rw+'
fo = open("foo.txt", "r+")
print "Name of the file: ", fo.name

# Assuming file has following 5 lines
# This is 1st line
# This is 2nd line
# This is 3rd line
# This is 4th line
# This is 5th line

line = fo.readlines(5) # prints the first string in each case, no matter what index is marked
print "Read Line: %s" % (line)

line = fo.readlines()
print "Read Line: %s" % (line) # prints whole string list starting from the second substring

line = fo.readlines(2) # prints nothing
print "Read Line: %s" % (line)

# Close opend file
fo.close()