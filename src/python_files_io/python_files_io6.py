#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_files_io.htm

# Open a file
fo = open("foo.txt", "r+")
str = fo.read(10); # reads only 10 first characters
#str = fo.read(); # reads full file
print "Read String is : ", str
# Close opend file
fo.close()
