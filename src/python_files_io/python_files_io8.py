#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_files_io.htm

import os

# Open/create a file
fo = open("test1.txt", "wb")
# Close opened/created file
fo.close()
# Rename a file from test1.txt to test2.txt
os.rename( "test1.txt", "test2.txt" )