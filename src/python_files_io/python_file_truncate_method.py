#!/usr/bin/python

# https://www.tutorialspoint.com/python/file_truncate.htm

# Open a file
#fo = open("foo.txt", "rw+") # ValueError: mode string must begin with one of 'r', 'w', 'a' or 'U', not 'rw+'
fo = open("foo.txt", "r+")
print "Name of the file: ", fo.name

# Assuming file has following 5 lines
# This is 1st line
# This is 2nd line
# This is 3rd line
# This is 4th line
# This is 5th line

line = fo.readline()
print "Read Line: %s" % (line)

# Now truncate remaining file.
fo.truncate()

# Try to read file now
line = fo.readline()
print "Read Line: %s" % (line) # after the first running wrote been truncated second string

# Close opend file
fo.close()