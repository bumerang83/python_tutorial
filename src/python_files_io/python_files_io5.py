#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_files_io.htm

# Open a file
fo = open("foo.txt", "wb")
fo.write( "Python is a great language.\nYeah its great!!\n");

# Close opend file
fo.close()
