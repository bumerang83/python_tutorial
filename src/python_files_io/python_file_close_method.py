#!/usr/bin/python

# https://www.tutorialspoint.com/python/file_close.htm

# Open a file
fo = open("foo.txt", "wb")
print "Name of the file: ", fo.name

# Close opend file
fo.close()
fo.close() # calling close() more than once is allowed