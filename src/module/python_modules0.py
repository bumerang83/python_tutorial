#!/usr/bin/python

# https://www.tutorialspoint.com/python/python_modules.htm

# Import module support, file 'support.py' in the current folder
import support

# Now you can call defined function that module as follows
support.print_func("Zara")
