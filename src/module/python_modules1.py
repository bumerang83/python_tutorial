#!/usr/bin/python

# https://www.tutorialspoint.com/python/python_modules.htm

Money = 2000
def AddMoney():
   # Uncomment the following line to fix the code:
   #global Money
   Money = 2 # assigning the start local value
   Money = Money + 1
   print Money

# this prints 2000, 2000 as uses global variable Money which was not changed
print Money
AddMoney()
print Money
